var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/User');

// Enregistre trois utilisateurs par défaut
router.get('/', function(req, res, next) {
  var riri = new User({ nom:'riri', key:'ririlepremier', email:'riri@donaldville.fr'});
  var fifi = new User({ nom:'fifi', key:'fifiledeuxieme', email:'fifi@donaldville.fr'});
  var loulou = new User({ nom:'loulou', key:'loulouletroisieme', email:'loulou@donaldville.fr'});
  
  console.log('Debut des enregistrements');
 
  riri.save(function(err) {
    if (err) throw err;
    console.log('riri enregistre !');
  })
 
  fifi.save(function(err) {
    if (err) throw err;
    console.log('fifi enregistre !');
  });
 
  loulou.save(function(err) {
    if (err) throw err;
    console.log('loulou enregistre !');
  });
 
  res.send('Enregistrements effectués...');
});

module.exports = router;
