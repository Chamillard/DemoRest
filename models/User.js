var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    nom: String,
    key: String,
    email: String
});

module.exports = mongoose.model('User', UserSchema, 'user');